const express = require('express');

const router = express.Router();

const reviewMiddlewares = require('./../middlewares/review.middleware');

// Group routes level.
router.use(reviewMiddlewares.commonMiddleware);

// GET List + route level middleware.
router.get('/',reviewMiddlewares.getAllReviews, (req,res) => {
    console.log('Get all reviews');

    res.json({
        message: 'Get All reviews'
    })
})

// Get detail
router.get('/:reviewid',reviewMiddlewares.getReviewById, (req,res) => {
    console.log('Get detail review by Id');

    res.json({
        message: 'Get detail review by Id'
    })
})

// Create new review
router.post('/', reviewMiddlewares.createReview, (req,res) => {
    console.log('Create new review');

    res.json({
        message: 'Create new review'
    })
})


// update a review
router.put('/:reviewid', reviewMiddlewares.updateReview, (req,res) => {
    console.log('Update a review');

    res.json({
        message: 'Update a review'
    })
})

// Delete a review
router.delete('/:reviewid', reviewMiddlewares.deleteReview, (req,res) => {
    console.log('Delete a review');

    res.json({
        message: 'Delete a review'
    })
})

// CommonJs way
module.exports = router;