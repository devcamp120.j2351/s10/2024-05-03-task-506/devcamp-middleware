const express = require('express');

const router = express.Router();
const courseMiddlewares = require('./../middlewares/course.middleware');

// Group routes
router.use(courseMiddlewares.commonMiddleware);

// GET List
router.get('/', courseMiddlewares.getAllCourses, (req,res) => {
    console.log('Get all courses');

    res.json({
        message: 'Get All courses'
    })
})

// Get detail
router.get('/:courseid', courseMiddlewares.getCourseById, (req,res) => {
    console.log('Get detail course by Id');

    res.json({
        message: 'Get detail course by Id'
    })
})

// Create new course
router.post('/', courseMiddlewares.createCourse, (req,res) => {
    console.log('Create new course');

    res.json({
        message: 'Create new course'
    })
})

// Update a course
router.put('/:courseid', courseMiddlewares.updateCourse, (req,res) => {
    console.log('Update a course');

    res.json({
        message: 'Update a course'
    })
})

// Delete a course
router.delete('/:courseid', courseMiddlewares.deleteCourse, (req,res) => {
    console.log('Delete a course');

    res.json({
        message: 'Delete a course'
    })
})

// CommonJs way
module.exports = router;