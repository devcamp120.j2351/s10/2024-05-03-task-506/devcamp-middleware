const commonMiddleware = (req, res, next) => {
    console.log('middleware: common for all course routes');
    next();
}


const getAllCourses = (req, res, next) => {
    console.log('middleware: get all courses');
    next();
}

const getCourseById = (req, res, next) => {
    console.log('middleware: get course by Id');
    next();
}

const createCourse = (req, res, next) => {
    console.log('middleware: create course');
    next();
}

const updateCourse = (req, res, next) => {
    console.log('middleware: update course by Id');
    next();
}

const deleteCourse = (req, res, next) => {
    console.log('middleware: delete course by Id');
    next();
}

module.exports = {
    getAllCourses,
    getCourseById,
    createCourse,
    updateCourse,
    deleteCourse,
    commonMiddleware
}