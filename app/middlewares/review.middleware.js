const commonMiddleware = (req, res, next) => {
    console.log('middleware: common for all review routes');
    next();
}


const getAllReviews = (req, res, next) => {
    console.log('middleware: get all reviews');
    next();
}

const getReviewById = (req, res, next) => {
    console.log('middleware: get review by Id');
    next();
}

const createReview = (req, res, next) => {
    console.log('middleware: create review');
    next();
}

const updateReview = (req, res, next) => {
    console.log('middleware: update review by Id');
    next();
}

const deleteReview = (req, res, next) => {
    console.log('middleware: delete review by Id');
    next();
}

module.exports = {
    getAllReviews,
    getReviewById,
    createReview,
    updateReview,
    deleteReview,
    commonMiddleware
}